FROM debian

# update sources list
RUN apt-get clean
RUN apt-get update

# install basic apps
RUN apt-get install -qy wget
RUN apt-get install -qy gawk
RUN apt-get install -qy vim
RUN apt-get install -qy git

RUN apt-get install -qy openmpi-bin libopenmpi-dev
RUN apt-get install -qy gfortran
RUN apt-get install -qy make
RUN apt-get install -qy libgsl-dev
RUN apt-get install -qy netcdf-bin libnetcdff-dev
RUN apt-get install -qy liblapack-dev
RUN apt-get install -qy python3
RUN apt-get install -qy python3-numpy
RUN apt-get install -qy python3-distutils
RUN apt-get install -qy python3-dev
RUN apt-get install -qy python3-matplotlib
RUN apt-get install -qy python3-mpi4py
RUN apt-get install -qy python3-scipy
RUN apt-get install -qy python3-sympy
RUN apt-get install -qy python3-pip
RUN apt-get install -qy python3-pyqt5
RUN apt-get install -qy pyqt5-dev
RUN apt-get install -qy texlive-latex-base
RUN apt-get install -qy texlive-latex-extra
RUN apt-get install -qy dvipng
RUN apt-get install -qy docker.io
RUN apt-get install -qy graphviz

#RUN apt-get install -qy python3-sphinx python3-sphinx-rtd-theme python3-sphinxcontrib.programoutput
# use pip to install new version of sphinx
RUN apt-get install -qy python3-venv
RUN python3 -m venv /venv --symlinks --system-site-packages
RUN /venv/bin/pip install sphinx sphinx-rtd-theme sphinxcontrib-programoutput
ENV VIRTUAL_ENV /venv
ENV PATH=/venv/bin:$PATH

# cleanup
RUN apt-get -qy autoremove
